\subsection*{Alphabet}

Láadan has its own set of sounds used within the language, as well
as its own euphony rules to give the language a certain flowing
quality. Not every sound in English is represented in Láadan,
but all but one sound in Láadan is used in English (the only exception
being \textit{``lh''}, which has a specific purpose - more on that later.)

\paragraph{There are five vowel sounds in Láadan:} ~\\

\begin{tabular}{l | c c c c c}
    \textbf{Letter} &
    \laadan{a} & 
    \laadan{e} & 
    \laadan{i} & 
    \laadan{o} & 
    \laadan{u} \\
    \textbf{Sound} &
    f\underline{a}ther &
    b\underline{e}ll &
    b\underline{i}g &
    h\underline{o}pe &
    m\underline{oo}n \\
    \textbf{IPA} &
    \includegraphics[width=0.75cm]{images/ipa-a.png} &
    \includegraphics[width=0.75cm]{images/ipa-e.png} &
    \includegraphics[width=0.75cm]{images/ipa-i.png} &
    \includegraphics[width=0.75cm]{images/ipa-o.png} &
    \includegraphics[width=0.75cm]{images/ipa-u.png} \\
\end{tabular} ~\\~\\

Note that the letter ``i'' has an ``ih'' sound; many languages use
``i'' for the ``EE'' sound, which is not a sound in Láadan.

\paragraph{And there are thirteen consonant sounds in Láadan:} ~\\

\begin{tabular}{l | c c c c c}
    \textbf{Letter} &
    \laadan{b} & 
    \laadan{d} & 
    \laadan{h} & 
    \laadan{l} & 
    \laadan{lh} \\
    \textbf{Sound} &
    a\underline{b}ack &
    \underline{d}ream &
    \underline{h}igh &
    wea\underline{l}th &
    * \\
    \textbf{IPA} &
    \includegraphics[width=0.75cm]{images/ipa-b.png} &
    \includegraphics[width=0.75cm]{images/ipa-d.png} &
    \includegraphics[width=0.75cm]{images/ipa-h.png} &
    \includegraphics[width=0.75cm]{images/ipa-l.png} &
    \includegraphics[width=0.75cm]{images/ipa-lh.png} \\ \\
    
    \textbf{Letter} &
    \laadan{m} & 
    \laadan{n} & 
    \laadan{r} & 
    \laadan{sh} & 
    \laadan{th} \\
    \textbf{Sound} &
    hi\underline{m} &
    mo\underline{n}th &
    \underline{r}ed &
    \underline{sh}ine &
    \underline{th}ink \\
    \textbf{IPA} &
    \includegraphics[width=0.75cm]{images/ipa-m.png} &
    \includegraphics[width=0.75cm]{images/ipa-n.png} &
    \includegraphics[width=0.75cm]{images/ipa-r.png} &
    \includegraphics[width=0.75cm]{images/ipa-sh.png} &
    \includegraphics[width=0.75cm]{images/ipa-th.png} \\ \\
    
    \textbf{Letter} &
    \laadan{w} & 
    \laadan{y} & 
    \laadan{zh} \\
    \textbf{Sound} &
    \underline{w}eep &
    \underline{y}ou &
    plea\underline{s}ure \\
    \textbf{IPA} &
    \includegraphics[width=0.75cm]{images/ipa-w.png} &
    \includegraphics[width=0.75cm]{images/ipa-y.png} &
    \includegraphics[width=0.75cm]{images/ipa-zh.png}
\end{tabular} ~\\~\\

Something I have noticed about the sounds of Láadan is that there
are no sharp sounds - like ``k'' and ``t''. Though it isn't stated
explicitly, I think that the consonants selected for the Láadan language
are meant to be ``soft''.
Suzette did state in The First Dictionary and Grammar of Láadan that
the sounds of the language were made to be ``simple to pronounce''
no matter what the native language of the learner. However, from my
own experience, certain sounds like ``th'' may be a bit tough;
in Hindi, ``th'' is an aspirated ``t'' sound, and I've noticed that
my husband has difficulty making that sound the way a native English
speaker would. ~\\

The only sound that is in Láadan that does not exist in English is
``lh''. Suzette describes its sound this way in A First Dictionary and Grammar of Láadan:

\begin{quote}
    If you put the tip of your tongue firmly against the roof of
    your mouth at the point where it begins to arch upward, draw the
    corners of your lips back as you would for an exaggerated smile,
    and try to say English ``sh'', the result should be an adequate ``lh''.
\end{quote}

This ``lh'' sound is kind of a hissing noise, used to add an explicit
negativity to words. It can be added to the beginning, ending, or middle
of words to make it extremely negative. For example, \laadan{ada} means
laugh, but adding that ``lh'' turns it into the word for ridicule or scorn:
\laadan{lhada}.

\subsection*{Accented letters}

In addition to the sounds themselves, Láadan has accented vowels,
which has a \textit{high tone}. Suzette had suggested that, if
you have trouble with tones, instead think of it as putting
emphasis on the letter - like telling the difference between the
verb ``convert'' (con-VERT) and the noun ``convert'' (CON-vert).

\begin{center}
\begin{tabular}{c c c c}
    Neutral tone & High tone & Low-then-high tone & High-then-low tone \\
    \laadan{o} & \laadan{ó} & \laadan{Loó} & \laadan{Lóo}
\end{tabular}
\end{center}

Still, it is not too difficult to remember to just raise your tone
a little bit when hitting an accented letter. It has specifically been
noted that Láadan does not have \textit{rising} and \textit{falling} 
tones,
\footnote{https://laadanlanguage.wordpress.com/misc-laadan-stuff-to-not-lose/laadan-problem/} 
simply high-then-low tone or low-then-high tone. For the word ``Láadan'',
it is specifically three syllables, not two: {Lá-a-dan}.

I, personally, tend to use this and the 
rising/falling tone ways interchangably. I happen to like the
tonal sounds of Mandarin Chinese, and it just seems somewhat natural
to speak it this way. However, mushing the two vowels into a single
sound removes that extra syllable. But, language usage inherently
changes the language. Nobody I know pronounces ``bottle'' with hard ``t''
sounds; they say it more like ``boddle''. Most people say ``wanna''
instead of ``want-to'', ``I-'unno'' instead of ``I-don't-know'', and so on.

\subsection*{Euphony rules}

There are some rules for putting together words in Láadan to account
for euphony. There are some exceptions in the language due to oversight
during the initial word creation. Language also evolves over time anyway,
so if Láadan is continually used there will probably be patterns that
arise that somewhat shift the sounds of the language.

\paragraph{Euphony rules:} ~\\

\begin{enumerate}
    \item   \textbf{Double-consonants must be split up by the letter ``e''.}
    \item   \textbf{Double-vowels must be split up with the letter ``h''.}
    \item   \textbf{Two vowels can be together only if they're the same letter
            and they have different accents (making a ``rising'' or ``falling'' tone.)}
    \item   \textbf{Words cannot end with ``h'', ``w'', ``y''}.
\end{enumerate}

For example, if you want to build the word for \textit{park}, \laadan{``heshehoth''},
you take the word for grass (\laadan{hesh}) and place (\laadan{hoth}) and combine
them - but, you need to split up the h letters with an ``e''.

\paragraph{Some exceptions:} ~\\

``bremeda'' (onion)

``ndi''

\subsection*{Láadan and technology}

Typing accented characters can be difficult for those of us in the U.S.,
who perhaps only speak and type in English, and have not worked with
other languages on their computers before. There are a few options for
writing Láadan on a computer.

\paragraph{Capitalize accented vowels:} One option, suggested by Suzette,
is to simply capitalize the accented vowel if you are not able to type
out the accent mark. So instead of Láadan, you would write LAadan.

\paragraph{Install an operating system keyboard:} If you're typing on
a computer, you can install keyboards for other languages. There are no
Láadan keyboards listed, but there are plenty of languages that use
the same vowel accent marks. There is a little bit of a learning curve
in figuring out how to switch the keyboard mode on the computer and
figuring out the keyboard shortcuts, but there are resources online
you can find.

\paragraph{Use the NumPad (in Windows):} You can also type out special
characters in Windows by holding down your \textbf{left ALT key}
and typing out the character code on your physical keyboard's numpad...

\begin{center}
\begin{tabular}{c c c c c c c c c c}
\laadan{á} & 
\laadan{Á} & 
\laadan{é} & 
\laadan{É} & 
\laadan{í} & 
\laadan{Í} & 
\laadan{ó} & 
\laadan{Ó} & 
\laadan{ú} & 
\laadan{Ú} \\
0225 & 
0193 &
0233 &
0201 &
0237 &
0205 &
0243 &
0211 &
0250 &
0218
\end{tabular}
\end{center}

\subsection*{Transliteration guide}

When creating a Láadan name for yourself, you could opt to take
the literal meaning of your name and try to translate it (e.g.,
``Rachel'' evidently means ``ewe''/female sheep, which would be \laadan{Éesh}),
or you could transliterate it, taking the sounds and trying to find
the closest equivalent in Láadan. There is no \textit{correct}
answer for how to transliterate your own name, it is ultimately up to you.

\paragraph{Letters that can stay the same:} ~\\

\begin{center}
\begin{tabular}{c c c c c c c c c}
b & d & h & l & m & n & r & th & w
\end{tabular}
\end{center}

\paragraph{Suggested letter substitutions:} ~\\

\begin{center}
\begin{tabular}{l l l l l}
    & & & & 
    \textbf{Example} \\
    \textbf{Letter} & 
    \textbf{Sound} & 
    \textbf{Sub suggestion} &
    \textbf{Example name} & 
    Transliteration*
    \\ \hline
    a   & ``ay''  & \laadan{e}      & R\underline{ay}      & \laadan{Ré}       \\
        & ``ah''  & \laadan{a}      & P\underline{a}t      & \laadan{Path}     \\
    e   & ``eh''  & \laadan{e}      & \underline{E}mma     & \laadan{Éma}     \\
        & ``E''   & \laadan{i}      & D\underline{ee}      & \laadan{Dí}  \\
        & ``uh''  & \laadan{a, e}   & Rach\underline{e}l   & \laadan{Réshal} \\
    i   & ``ih''  & \laadan{i}      & Ol\underline{i}via          & \laadan{Olibiha} \\
        & ``E''   & \laadan{i}      & Carr\underline{i}           & \laadan{Sharí} \\
        & ``I''   & \laadan{i}      & M\underline{i}chael         & \laadan{Míhel} \\
    o   & ``oh''  & \laadan{o}      & R\underline{o}se            & \laadan{Rósh} \\
        & ``aw''  & \laadan{a, o}   & \underline{O}ctavia      & \laadan{Athábiha} \\
        & ``uh''  & \laadan{a, e}   & Jac\underline{o}b        & \laadan{Záahab} \\
    u   & ``ooh'' & \laadan{u}      & S\underline{u}zette         & \laadan{Súzheth} \\
        & ``yew'' & \laadan{yu, u}  & \underline{U}lysses     & \laadan{Yúlise} \\ \hline
        
    c   & ``s''     & \laadan{sh}     & \underline{C}ecilia   & \laadan{Sheshílíha} \\
        & ``ch''    & \laadan{sh}     & \underline{Ch}arlie   & \laadan{Shalí} \\
        & ``sh''    & \laadan{sh}     & \underline{C}arlotte  & \laadan{Shálet} \\
        & ``k''     & \laadan{h, sh,} $\emptyset$   & \underline{Ch}loe     & \laadan{Lohí} \\
    f   & ``f''     & \laadan{h, sh}  & \underline{F}iona     & \laadan{Lihoná}   \\
    g   & like girl & \laadan{h, zh,} $\emptyset$ & \underline{G}abrielle & \laadan{Habíhel} \\
        & like gem  & \laadan{zh}   & \underline{G}em       & \laadan{Zhim} \\
    j   & like gem  & \laadan{zh}   & \underline{J}ennifer  & \laadan{Zhenihá} \\
    k   & ``k''     & \laadan{h, sh,} $\emptyset$ & \underline{K}arol     & \laadan{Háral} \\
    p   & ``p''     & \laadan{b}    & \underline{P}etra     & \laadan{Béthara} \\
    q   & ``k''     & \laadan{h, sh,} $\emptyset$ & \underline{Q}uinn     & \laadan{Wuhín} \\
    s   & ``s''     & \laadan{sh}   & \underline{S}uzanne   & \laadan{Shúzhan} \\
        & ``z''     & \laadan{zh}   & Eli\underline{s}abeth & \laadan{Elízhabeth} \\
    t   & ``t''     & \laadan{th}   & \underline{T}amara    & \laadan{Thamara} \\
    v   & ``v''     & \laadan{b, w} & \underline{V}ania     & \laadan{Bánaya} \\
    x   & ``ks''    & \laadan{h, sh,} $\emptyset$ & Pa\underline{x}ton    & \laadan{Báhatan} \\
        & ``z''     & \laadan{zh}   & \underline{X}ena      & \laadan{Zhína} \\
    y   & ``y''     & \laadan{y}    & \underline{Y}olanda   & \laadan{Yolána} \\
    z   & ``z''     & \laadan{zh}   & \underline{Z}oe       & \laadan{Zhohí} \\
\end{tabular}
\end{center}

\subsection*{Exercises}

    Practice these basic conversational phrases out loud. Transliterate
    the names of some of your friends and family and use those as well.
    
\begin{center}
\begin{tabular}{l l l l}
    \textbf{English} & \textbf{Láadan} & \textbf{Literal translation} \\ \hline
    Hello!          & \laadan{Wil sha!} & \textit{Let there be harmony!} \\
    Bye!            & \laadan{Aril!}    & \textit{Later!} \\
    How are you?    & \laadan{Báa thal ne?} & \textit{Are you good?} \\
    I'm good.       & \laadan{[Bíi] thal le [wa.]} \\
    I'm tired.      & \laadan{[Bíi] óoha le [wa.]} 
\end{tabular}
\end{center}
    
    
    
